[
    {
        "id": 271,
        "question": "What are the disadvantages of WordPress?",
        "description": null,
        "answers": {
            "answer_a": "Only utilizes PHP",
            "answer_b": "Use of multiple plugins can make the website heavy to load and slow",
            "answer_c": "Modifying images and tables is difficult.",
            "answer_d": "All of the mentioned.",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "false",
            "answer_c_correct": "false",
            "answer_d_correct": "true",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "WordPress"
            }
        ],
        "category": "CMS",
        "difficulty": "Easy"
    },
    {
        "id": 140,
        "question": "What is the correct HTML for making a text area?",
        "description": null,
        "answers": {
            "answer_a": "<input type=\"textbox\">",
            "answer_b": "<textarea>",
            "answer_c": "<input type=\"textarea\">",
            "answer_d": "<textbox>",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "true",
            "answer_c_correct": "false",
            "answer_d_correct": "false",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_b",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "HTML"
            }
        ],
        "category": "",
        "difficulty": "Easy"
    },
    {
        "id": 291,
        "question": "Which relational database does WordPress use?",
        "description": null,
        "answers": {
            "answer_a": "Oracle",
            "answer_b": "MySQL",
            "answer_c": "PostgresSQL",
            "answer_d": "MSSQL SERVER",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "true",
            "answer_c_correct": "false",
            "answer_d_correct": "false",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "WordPress"
            }
        ],
        "category": "CMS",
        "difficulty": "Easy"
    },
    {
        "id": 720,
        "question": "Cronjobs in kubernetes run in",
        "description": null,
        "answers": {
            "answer_a": "UTC only",
            "answer_b": "Based on NTP settings",
            "answer_c": "Master node local timezone",
            "answer_d": "GMT only",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "true",
            "answer_b_correct": "false",
            "answer_c_correct": "false",
            "answer_d_correct": "false",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "Kubernetes"
            }
        ],
        "category": "Linux",
        "difficulty": "Easy"
    },
    {
        "id": 731,
        "question": "How to compares the current state of the cluster against the state that the cluster would be in if the manifest was applied in Kubernetes?",
        "description": null,
        "answers": {
            "answer_a": "kubectl show -f ./my-manifest.yaml",
            "answer_b": "kubectl log -f ./my-manifest.yaml",
            "answer_c": "kubectl state -f ./my-manifest.yaml",
            "answer_d": "kubectl diff -f ./my-manifest.yaml",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "false",
            "answer_c_correct": "false",
            "answer_d_correct": "true",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "Kubernetes"
            }
        ],
        "category": "Linux",
        "difficulty": "Easy"
    },
    {
        "id": 945,
        "question": "Which of the following commands would show you the status of a Kubernetes deployment?",
        "description": null,
        "answers": {
            "answer_a": "kubectl rollout stats deployment/Deployment",
            "answer_b": "kubectl rollout status deployment/Deployment",
            "answer_c": "kubectl rollout log deployment/Deployment",
            "answer_d": null,
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "true",
            "answer_c_correct": "false",
            "answer_d_correct": "false",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": null,
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "Kubernetes"
            }
        ],
        "category": "DevOps",
        "difficulty": "Medium"
    },
    {
        "id": 916,
        "question": "What are the responsibilities of Replication Controller?",
        "description": null,
        "answers": {
            "answer_a": "Update or delete multiple pods with a single command",
            "answer_b": "Helps to achieve the desired state",
            "answer_c": "Creates a new pod, if the existing pod crashes",
            "answer_d": "All of the mentioned",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "false",
            "answer_c_correct": "false",
            "answer_d_correct": "true",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": null,
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "Kubernetes"
            }
        ],
        "category": "DevOps",
        "difficulty": "Medium"
    },
    {
        "id": 388,
        "question": "Which is the log in which data changes received from a replication master server are written?",
        "description": null,
        "answers": {
            "answer_a": "Error Log",
            "answer_b": "Relay Log",
            "answer_c": "General Query Log",
            "answer_d": "Binary Log",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "true",
            "answer_c_correct": "false",
            "answer_d_correct": "false",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "MySQL"
            }
        ],
        "category": "SQL",
        "difficulty": "Medium"
    },
    {
        "id": 501,
        "question": "Tags and text that are not directly displayed on the page are written in _____ section.",
        "description": null,
        "answers": {
            "answer_a": "<html>",
            "answer_b": "<body>",
            "answer_c": "<title>",
            "answer_d": "<head>",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "false",
            "answer_c_correct": "false",
            "answer_d_correct": "true",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "HTML"
            }
        ],
        "category": "Code",
        "difficulty": "Medium"
    },
    {
        "id": 633,
        "question": "What type of function is needed when passing values through a form or an URL?",
        "description": null,
        "answers": {
            "answer_a": "htmlspecialchars()",
            "answer_b": "urlencode()",
            "answer_c": "Both of these",
            "answer_d": "None of the mentioned",
            "answer_e": null,
            "answer_f": null
        },
        "multiple_correct_answers": "false",
        "correct_answers": {
            "answer_a_correct": "false",
            "answer_b_correct": "false",
            "answer_c_correct": "true",
            "answer_d_correct": "false",
            "answer_e_correct": "false",
            "answer_f_correct": "false"
        },
        "correct_answer": "answer_a",
        "explanation": null,
        "tip": null,
        "tags": [
            {
                "name": "PHP"
            }
        ],
        "category": "Code",
        "difficulty": "Medium"
    }
]