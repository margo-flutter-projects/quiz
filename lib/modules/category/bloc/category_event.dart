abstract class CategoryEvent {}

class ChoiceCategoryEvent extends CategoryEvent {
  final String value;

  ChoiceCategoryEvent({required this.value});
}
