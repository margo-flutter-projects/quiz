import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quiz/modules/category/bloc/category_event.dart';
import 'package:quiz/modules/category/bloc/category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc() : super(ChoiceCategoryState()) {
    on<ChoiceCategoryEvent>((event, emit) async {
      try {
        int indexCategory = state.indexCategory;
        int indexDifficulty = state.indexDifficulty;
        if (Parameters.categoryList.contains(event.value)) {
          //выбрана категория
          indexCategory = Parameters.categoryList.indexOf(event.value);
        } else if (Parameters.difficultyList.contains(event.value)) {
          //выбрана сложность
          indexDifficulty = Parameters.difficultyList.indexOf(event.value);
        }
        emit(ChoiceCategoryState(
          indexCategory: indexCategory,
          indexDifficulty: indexDifficulty,
        ));
      } catch (error) {
        emit(
          ErrorCategoryState(
            error.toString(),
            indexCategory: state.indexCategory,
            indexDifficulty: state.indexDifficulty,
          ),
        );
      }
    });
  }
}
