import 'package:bloc/bloc.dart';
import 'package:quiz/modules/question/bloc/question_event.dart';
import 'package:quiz/modules/question/bloc/question_state.dart';
import 'package:quiz/question.dart';
import 'package:quiz/requests/get_questions_list.dart';

class QuestionBloc extends Bloc<QuestionEvent, QuestionState> {
  QuestionBloc() : super(LoadingQuestionState()) {
    on<LoadingQuestionEvent>((event, emit) async {
      try {
        String category = event.category;
        String difficulty = event.difficulty;
        List<Question> questions = await getQuestionsList(category, difficulty);
        if (questions.isEmpty) {
          emit(EmptyQuestionState());
        } else {
          emit(
            AnswersQuestionState(questions: questions),
          );
        }
      } catch (error) {
        emit(ErrorLoadingQuestionState(error.toString()));
      }
    });

    on<AnswerQuestionEvent>((event, emit) async {
      bool allCorrect = true;
      try {
        //если юзер ошибся
        List<bool?> answers = state.answers;
        if (event.userAnswersMap.containsValue(false)) {
          answers[state.index] = false;
          emit(AnswersQuestionState(
            questions: state.questions!,
            index: state.index < 9 ? state.index + 1 : state.index,
            myAnswers: answers,
          ));
        }
        //проверим все ли правильные ответы щелкнуты юзером
        event.correctAnswersMap.forEach((key, value) {
          if (value && event.userAnswersMap[key] != true) {
            allCorrect = false;
          }
        });

        if (allCorrect) {
          answers[state.index] = true;
          emit(AnswersQuestionState(
            questions: state.questions!,
            index: state.index < 9 ? state.index + 1 : state.index,
            myAnswers: answers,
          ));
          state.answers.where((element) => element == true);
        } else {
          emit(AnswersQuestionState(
            questions: state.questions!,
            index: state.index,
            myAnswers: answers,
            userAnswersMap: event.userAnswersMap,
          ));
        }
      } catch (error) {
        emit(AnswersQuestionState(
          questions: state.questions!,
          errorText: error.toString(),
        ));
      }
    });

    on<AnswerQuestionEvent2>((event, emit) async {
      try {
        state.answers[state.index] = event.answer;
        emit(
          AnswersQuestionState(
            questions: state.questions!,
            index: state.index < 9 ? state.index + 1 : state.index,
            myAnswers: state.answers,
          ),
        );
      } catch (error) {
        emit(AnswersQuestionState(
          questions: state.questions!,
          errorText: error.toString(),
        ));
      }
    });
  }
}
