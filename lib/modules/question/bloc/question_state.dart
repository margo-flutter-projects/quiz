import 'package:quiz/question.dart';

abstract class QuestionState {
  final List<Question>? questions;
  final List<bool?> answers;
  final int index;

  QuestionState({
    this.questions,
    List<bool?>? myAnswers,
    int? index,
  })  : answers = myAnswers ?? List<bool?>.filled(10, null),
        index = index ?? 0;
}

class LoadingQuestionState extends QuestionState {}

class EmptyQuestionState extends QuestionState {}

class AnswersQuestionState extends QuestionState {
  final String? errorText;
  final Map<String, bool?> userCorrectAnswersMap;

  AnswersQuestionState(
      {required List<Question> questions,
      List<bool?>? myAnswers,
      int? index,
      Map<String, bool?>? userAnswersMap,
      this.errorText})
      : userCorrectAnswersMap = userAnswersMap ??
            {
              'a': null,
              'b': null,
              'c': null,
              'd': null,
              'e': null,
              'f': null,
            },
        super(
          questions: questions,
          myAnswers: myAnswers,
          index: index,
        );
}

class AnswersQuestionState2 extends QuestionState {}

class ErrorLoadingQuestionState extends QuestionState {
  final String errorText;

  ErrorLoadingQuestionState(this.errorText);
}
