import 'package:flutter/material.dart';
import 'package:quiz/modules/category/category_page.dart';
import 'package:quiz/modules/widgets/main_button.dart';

class ScorePage extends StatelessWidget {
  const ScorePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int correctAnswers =
        ModalRoute.of(context)!.settings.arguments as int;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Score"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Column(
          children: [
            Expanded(
              child: Center(
                child: Text(
                  '$correctAnswers / 10',
                  style: const TextStyle(fontSize: 26),
                ),
              ),
            ),
            MainButton(
              "End",
              onPressed: () => Navigator.restorablePushNamedAndRemoveUntil(
                  context, '/', (route) => false),
            ),
          ],
        ),
      ),
    );
  }
}
