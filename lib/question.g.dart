// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Question _$QuestionFromJson(Map<String, dynamic> json) => Question(
      json['id'] as int,
      json['question'] as String,
      Answers.fromJson(json['answers'] as Map<String, dynamic>),
      json['correct_answer'] as String?,
      json['explanation'] as String?,
      (json['tags'] as List<dynamic>)
          .map((e) => Tag.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['category'] as String?,
      json['difficulty'] as String,
      stringToBool(json['multiple_correct_answers'] as String?),
      json['description'] as String?,
      CorrectAnswers.fromJson(json['correct_answers'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$QuestionToJson(Question instance) => <String, dynamic>{
      'id': instance.id,
      'question': instance.question,
      'description': instance.description,
      'answers': instance.answers,
      'multiple_correct_answers': instance.multipleCorrectAnswers,
      'correct_answers': instance.correctAnswers,
      'correct_answer': instance.correctAnswer,
      'explanation': instance.explanation,
      'tags': instance.tags,
      'category': instance.category,
      'difficulty': instance.difficulty,
    };

Answers _$AnswersFromJson(Map<String, dynamic> json) => Answers(
      json['answer_a'] as String?,
      json['answer_b'] as String?,
      json['answer_c'] as String?,
      json['answer_d'] as String?,
      json['answer_e'] as String?,
      json['answer_f'] as String?,
    );

Map<String, dynamic> _$AnswersToJson(Answers instance) => <String, dynamic>{
      'answer_a': instance.a,
      'answer_b': instance.b,
      'answer_c': instance.c,
      'answer_d': instance.d,
      'answer_e': instance.e,
      'answer_f': instance.f,
    };

CorrectAnswers _$CorrectAnswersFromJson(Map<String, dynamic> json) =>
    CorrectAnswers(
      stringToBool(json['answer_a_correct'] as String?),
      stringToBool(json['answer_b_correct'] as String?),
      stringToBool(json['answer_c_correct'] as String?),
      stringToBool(json['answer_d_correct'] as String?),
      stringToBool(json['answer_e_correct'] as String?),
      stringToBool(json['answer_f_correct'] as String?),
    );

Map<String, dynamic> _$CorrectAnswersToJson(CorrectAnswers instance) =>
    <String, dynamic>{
      'answer_a_correct': instance.a,
      'answer_b_correct': instance.b,
      'answer_c_correct': instance.c,
      'answer_d_correct': instance.d,
      'answer_e_correct': instance.e,
      'answer_f_correct': instance.f,
    };

Tag _$TagFromJson(Map<String, dynamic> json) => Tag(
      json['name'] as String,
    );

Map<String, dynamic> _$TagToJson(Tag instance) => <String, dynamic>{
      'name': instance.name,
    };
